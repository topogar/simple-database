CREATE OR REPLACE VIEW VIEW_PARTNER AS (
  SELECT
    P.partner_nm AS partner_nm
  , CASE
      WHEN P.specialization_code = 1 THEN
        'gas station'
      WHEN P.specialization_code = 2 THEN
        'electric station'
      WHEN P.specialization_code = 3 THEN
        'car wash'
      ELSE
        NULL
    END AS partner_specialization
  FROM
    PARTNER P
  WHERE
    P.specialization_code != 0
);

/*К character и participant*/

CREATE OR REPLACE VIEW character_participant AS (
    SELECT DISTINCT
      chrt.first_nm name_chrt
      , (CASE
           WHEN
             chrt.second_nm IS NULL THEN 'Нет фамилии, и не предполагается'
           ELSE
             chrt.second_nm
        END) AS second_name_chrt
      , prt.first_nm name_actor
      , prt.second_nm second_name_actor
    FROM
      character chrt
    INNER JOIN
      participant prt
    ON
      chrt.participant_id = prt.participant_id
);

DROP VIEW character_participant;

SELECT *
   FROM character_participant;

/*К cinematic_universe */

CREATE OR REPLACE VIEW year_cin_un AS (
    SELECT DISTINCT
      cinematic_universe_nm
      , MIN(Extract(YEAR from(premier_dt))) AS year_beginning
    FROM
      cinematic_universe AS cu
    INNER JOIN
      movie              AS mv
    ON
      cu.cinematic_universe_id = mv.cinematic_universe_id
    GROUP BY cu.cinematic_universe_id
);

DROP VIEW year_cin_un;

SELECT *
   FROM year_cin_un;

/* К movie */

CREATE OR REPLACE VIEW